#![feature(async_closure)]
#![deny(unsafe_code)]
// warnings are ok during development, but have to be explicitly allowed at usage site if required during release
// #![cfg_attr(not(debug_assertions), deny(warnings))]
#![warn(clippy::pedantic)]
#![warn(clippy::nursery)]
#![warn(clippy::cargo)]
#![allow(clippy::cargo_common_metadata)]
// this is triggered by external dependencies and can't be fixed by us
#![allow(clippy::multiple_crate_versions)]
// sorry, good documentation is yet-to-come
#![allow(clippy::missing_panics_doc)]

mod init;

use anyhow::{bail, Context, Result};
use arrayvec::ArrayVec;
use futures::{Sink, SinkExt, Stream, StreamExt};
use init::init;
use log::{info, warn};
use std::{error::Error, future, pin::Pin, time::Duration};
use tokio::time::timeout;
use tokio_socketcan::{CANFrame, CANSocket};

struct Comm<Rx, Tx>
where
	Rx: Stream<Item = CANFrame> + Send,
	Tx: Sink<CANFrame> + Send,
{
	rx: Pin<Box<Rx>>,
	tx: Pin<Box<Tx>>,
}

impl<Rx, Tx, Err> Comm<Rx, Tx>
where
	Rx: Stream<Item = CANFrame> + Send,
	Tx: Sink<CANFrame, Error = Err> + Send,
	Err: Error + Send + Sync + 'static,
{
	pub async fn request<const RESPONSE_LEN: usize>(
		mut self: Pin<&mut Self>,
		id: u32,
		data: &[u8],
	) -> Result<[CANFrame; RESPONSE_LEN]> {
		self.tx.send(CANFrame::new(id, data, true, false)?).await?;

		let mut response = ArrayVec::<_, RESPONSE_LEN>::new();

		while let Ok(frame) = timeout(Duration::from_millis(50), async {
			let res: Result<CANFrame> = Ok(loop {
					let frame = self
						.rx
						.next()
						.await
						.context("reached the end of the frame stream")?;

					if !frame.is_rtr() && frame.id() == id {
						break frame;
					}/*  else {
						info!("received non-matching frame: {:x?}", frame)
					} */
				});
			res
		})
		.await
		{
			response
				.try_push(frame?)
				.context("received more response packets than expected")?
		}

		match response.into_inner() {
			Ok(array) => Ok(array),
			Err(response) => {
				bail!(
					"received too few response frames. expected={}, received={}",
					RESPONSE_LEN,
					response.len()
				)
			}
		}
	}
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
	let config = init();

	let socket =
		|| -> Result<CANSocket, Box<dyn Error>> { Ok(CANSocket::open(&config.interface)?) };
	let new_rx = || -> Result<_, Box<dyn Error>> {
		Ok(socket()?
			.then(async move |frame| frame.unwrap())
			.filter(|frame| future::ready(!matches!(frame.id(), 0x517 | 0x5f1 | 0x500..=0x50d))))
	};

	let mut comm = Comm {
		rx: Box::pin(new_rx()?),
		tx: Box::pin(socket()?),
	};
	let comm = Pin::new(&mut comm);

	// let echo_future = new_rx()?.for_each(async move |frame| {
	// 	info!("frame: {:x?}", frame);
	// });

	info!("sending.");
	let res = comm.request::<1>(0x51e, &[]).await?;
	info!("sent. res={:x?}", res);

	// echo_future.await;
	Ok(())
}
