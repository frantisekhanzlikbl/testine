use clap::Clap;
use std::env;

const RUST_LOG: &str = "RUST_LOG";

#[derive(Clap)]
#[clap(
	version = "0.0.0",
	author = "František Hanzlík <frantisekhanzlikbl@gmail.com>"
)]
pub struct GlobalOpts {
	/// the network interface to use for communication
	#[clap(short, long, default_value = "slcan0")]
	pub interface: String,

	/// how long to wait for some activity on the bus before timing out
	#[clap(long, default_value = "2000")]
	pub timeout_duration_msec: u64,

	/// how many consecutive timeouts are allowed before crashing due to lack of activity on the bus.
	#[clap(long, default_value = "5")]
	pub allowed_timeouts: i32,
}

pub fn init() -> GlobalOpts {
	if env::var_os(RUST_LOG).is_none() {
		env::set_var(RUST_LOG, "main=info");
	}
	pretty_env_logger::init_custom_env(RUST_LOG);

	GlobalOpts::parse()
}
