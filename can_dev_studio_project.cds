{
    "connections": [
        {
            "in_id": "{c0197ee6-dd7e-4d8c-bb2f-204121466502}",
            "in_index": 0,
            "out_id": "{23b74ebc-6b4e-420a-bc34-40c88b4a9376}",
            "out_index": 0
        },
        {
            "in_id": "{23b74ebc-6b4e-420a-bc34-40c88b4a9376}",
            "in_index": 0,
            "out_id": "{015b2e0e-c4bc-43f7-adfe-f471ac355eab}",
            "out_index": 0
        },
        {
            "in_id": "{015b2e0e-c4bc-43f7-adfe-f471ac355eab}",
            "in_index": 0,
            "out_id": "{68388556-049f-4b75-9d27-75e894bcc4c9}",
            "out_index": 0
        }
    ],
    "nodes": [
        {
            "id": "{68388556-049f-4b75-9d27-75e894bcc4c9}",
            "model": {
                "caption": "sender",
                "content": [
                    {
                        "data": "3100000000000000",
                        "id": "5ff",
                        "interval": "",
                        "loop": false,
                        "remote": false,
                        "send": false
                    },
                    {
                        "data": "",
                        "id": "5f2",
                        "interval": "",
                        "loop": false,
                        "remote": true,
                        "send": false
                    },
                    {
                        "data": "",
                        "id": "5f3",
                        "interval": "",
                        "loop": false,
                        "remote": true,
                        "send": false
                    }
                ],
                "name": "CanRawSender",
                "senderColumns": [
                    "Id",
                    "Data",
                    "Remote",
                    "Loop",
                    "Interval",
                    ""
                ],
                "sorting": {
                    "currentIndex": 0
                }
            },
            "position": {
                "x": 162.67200000000014,
                "y": 459.27600000000007
            }
        },
        {
            "id": "{c0197ee6-dd7e-4d8c-bb2f-204121466502}",
            "model": {
                "caption": "out view",
                "name": "CanRawView",
                "scrolling": false,
                "viewColumns": [
                    {
                        "name": "time",
                        "vIdx": 1
                    },
                    {
                        "name": "id",
                        "vIdx": 2
                    },
                    {
                        "name": "dir",
                        "vIdx": 3
                    },
                    {
                        "name": "len",
                        "vIdx": 4
                    },
                    {
                        "name": "data",
                        "vIdx": 5
                    }
                ]
            },
            "position": {
                "x": 982.8159999999998,
                "y": 477.55600000000015
            }
        },
        {
            "id": "{23b74ebc-6b4e-420a-bc34-40c88b4a9376}",
            "model": {
                "caption": "filter",
                "name": "CanRawFilter",
                "rxList": [
                    {
                        "id": "517",
                        "payload": ".*",
                        "policy": false
                    },
                    {
                        "id": "5f1",
                        "payload": ".*",
                        "policy": false
                    },
                    {
                        "id": "50[a-d0-9]",
                        "payload": ".*",
                        "policy": false
                    },
                    {
                        "id": ".*",
                        "payload": ".*",
                        "policy": true
                    }
                ],
                "txList": [
                    {
                        "id": ".*",
                        "payload": ".*",
                        "policy": true
                    }
                ]
            },
            "position": {
                "x": 650,
                "y": 431
            }
        },
        {
            "id": "{015b2e0e-c4bc-43f7-adfe-f471ac355eab}",
            "model": {
                "backend": "socketcan",
                "caption": "battery",
                "configuration": "",
                "interface": "slcan0",
                "name": "CanDevice"
            },
            "position": {
                "x": 404.58681481481494,
                "y": 429.30555555555543
            }
        }
    ]
}
